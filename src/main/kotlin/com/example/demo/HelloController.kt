package com.example.demo

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController


/**
 *
 * @author ksh
 * @since 2018.
 */

@RestController
class HelloController {
    @GetMapping("/")
    fun getHelloUniverse() = "Hello World"
}